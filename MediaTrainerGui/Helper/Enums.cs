﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaTrainerGui.Helper
{
    public enum QuestionResults { Unanswered=-1, WrongAnswer=0, CorrectAnswer=1}
}
