﻿using MediaTrainerGui.Helper;
using MediaTrainerGui.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaTrainerGui.ViewModels
{
    public abstract class ViewModelQuestion: INotifyPropertyChanged
    {
        public virtual Question Question { get; set; }

        private string __DirectoryPath;

        public string Text
        {
            get
            {
                return Question.Text;
            }
            set
            {
                if (value != Question.Text)
                {
                    Question.Text = value;
                    RaisePropertyChanged("Text");
                }
            }
        }

        public string AudioPath
        {
            get
            {
                if (Question.AudioPath == null)
                {
                    return "";
                }
                return Path.Combine(__DirectoryPath, Question.AudioPath);
            }
        }

        public string ImagePath
        {
            get
            {
                if (Question.ImagePath == null)
                {
                    return "";
                }
                return Path.Combine(__DirectoryPath, Question.ImagePath);
            }
        }

        public string ImageCaption
        {
            get
            {
                return Question.ImageCaption;
            }
        }

        public int Index { get; set; }

        #region Constructors

        public ViewModelQuestion(Question question, int index, string DirectoryPath)
        {
            Question = question;
            Index = index;
            __DirectoryPath = DirectoryPath;
        }

        #endregion Constructors

        public void Reset()
        {
            Question.Reset();
        }

        public QuestionResults CorrectAnswer()
        {
            return Question.AnswerResult();
        }

        internal void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
