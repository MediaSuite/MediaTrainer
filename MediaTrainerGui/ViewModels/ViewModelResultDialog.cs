﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediaTrainerGui.Models;

namespace MediaTrainerGui.ViewModels
{
    public class ViewModelResultDialog : INotifyPropertyChanged
    {
        public int NotAnswered
        {
            get
            {
                return __Result.NotAnswered;
            }
        }

        public int CorrectAnswered
        {
            get
            {
                return __Result.CorrectAnswered;
            }
        }

        public int FalseAnswered
        {
            get
            {
                return __Result.FalseAnswered;
            }
        }

        public int NumberQuestions
        {
            get
            {
                return __Result.NumberQuestions;
            }
        }

        public int Percentage
        {
            get
            {
                return __Result.Percentage;
            }
        }

        public DateTime TimeStamp
        {
            get
            {
                return __Result.TimeStamp;
            }
        }

        public string Name
        {
            get
            {
                return __Result.Name;
            }
        }

        private Result __Result { set; get; }

        public ViewModelResultDialog(Result result)
        {
            __Result = result;
        }

        internal void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
