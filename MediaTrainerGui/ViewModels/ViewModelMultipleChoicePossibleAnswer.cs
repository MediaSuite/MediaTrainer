﻿using MediaTrainerGui.Helpers;
using MediaTrainerGui.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MediaTrainerGui.ViewModels
{
    public class ViewModelMultipleChoicePossibleAnswer : INotifyPropertyChanged
    {
        private ViewModelMultipleChoiceQuestion __viewModelMultipleChoiceQuestion;

        public RelayCommand AnswerCommand { get; set; }

        private string __Text;
        public string Text
        {
            get
            {
                return __Text;
            }
        }

        private int __Index;

        public Brush BackgroundColor
        {
            get
            {
                if (__viewModelMultipleChoiceQuestion.Answer != null)
                {
                    if (__viewModelMultipleChoiceQuestion.Answer == __Index && __viewModelMultipleChoiceQuestion.Answer == __viewModelMultipleChoiceQuestion.Question.CorrectAnswerIndex)
                    {
                        return Brushes.LightGreen;
                    }
                    else if (__viewModelMultipleChoiceQuestion.Answer == __Index && __viewModelMultipleChoiceQuestion.Answer != __viewModelMultipleChoiceQuestion.Question.CorrectAnswerIndex)
                    {
                        return Brushes.Red;
                    }
                    else if (__Index == __viewModelMultipleChoiceQuestion.Question.CorrectAnswerIndex)
                    {
                        return Brushes.DodgerBlue;
                    }
                    else
                    {
                        return Brushes.LightGray;
                    }


                }
                return Brushes.LightGray;
            }
        }

        public ViewModelMultipleChoicePossibleAnswer(ViewModelMultipleChoiceQuestion viewModelMultipleChoiceQuestion, string text, int index)
        {
            __viewModelMultipleChoiceQuestion = viewModelMultipleChoiceQuestion;
            __Text = text;
            __Index = index;
            AnswerCommand = new RelayCommand(AnswerQuestion);
        }

        void AnswerQuestion(object parameter)
        {
            if (parameter == null)
            {
                return;
            }
            __viewModelMultipleChoiceQuestion.Answer = __Index;
            RaisePropertyChanged("BackgroundColor");
        }

        internal void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
