﻿using MediaTrainerGui.Helpers;
using MediaTrainerGui.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaTrainerGui.ViewModels
{
    public class ViewModelMultipleChoiceQuestion : ViewModelQuestion
    {
        public ObservableCollection<ViewModelMultipleChoicePossibleAnswer> ViewModelsPossibleAnswers { get; set; }

        public int? Answer
        {
            get
            {
                return __Question.Answer;
            }
            set
            {
                if (__Question.Answer != value)
                {
                    __Question.Answer = value;
                    RaisePropertyChanged("ViewModelsPossibleAnswers");
                    foreach (var answer in ViewModelsPossibleAnswers)
                    {
                        answer.RaisePropertyChanged("BackgroundColor");
                    }
                }
            }
        }

        private MultipleChoiceQuestion __Question;
        public new MultipleChoiceQuestion Question
        {
            get
            {
                return __Question;
            }
        }

        public ViewModelMultipleChoiceQuestion(MultipleChoiceQuestion question, int index, string directoryPath) : base(question, index, directoryPath)
        {
            __Question = question;
            ViewModelsPossibleAnswers = new ObservableCollection<ViewModelMultipleChoicePossibleAnswer>();
            int j = 0;
            foreach (string Text in question.PossibleAnswers)
            {
                ViewModelsPossibleAnswers.Add(new ViewModelMultipleChoicePossibleAnswer(this, Text, j));
                j++;
            }
        }
    }
}
