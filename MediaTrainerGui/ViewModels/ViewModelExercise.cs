﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using MediaTrainerGui.Helpers;
using MediaTrainerGui.Models;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using MediaTrainerGui.Helper;
using System.IO;

namespace MediaTrainerGui.ViewModels
{
    public class ViewModelExercise: INotifyPropertyChanged
    {
        private ViewModelMain _ViewModelMain;
        public Exercise Exercise;
        public Window Window { get; set; }

        public RelayCommand PreviousQuestionCommand { get; set; }
        public RelayCommand NextQuestionCommand { get; set; }

        public StringToVisibilityConverter stringToVisibilityConverter;

        private ViewModelQuestion _CurrentQuestion;
        public ViewModelQuestion CurrentQuestion
        {
            get
            {
                return _CurrentQuestion;
             }
            set
            {
                if (value != _CurrentQuestion)
                {
                    _CurrentQuestion = value;
                    RaisePropertyChanged("CurrentQuestion");
                    AudioPath = _CurrentQuestion.AudioPath;
                }
            }

        }

        private string __AudioPath;
        public string AudioPath
        {
            get
            {
                return __AudioPath;
            }
            set
            {
                if (value != __AudioPath)
                {
                    __AudioPath = value;
                    RaisePropertyChanged("AudioPath");
                }
            }
        }

        public string Name
        {
            get
            {
                return Exercise.Name;
            }
        }

        public bool ExamMode
        {
            get
            {
                return Exercise.ExamMode;
            }
            set
            {
                if (value != Exercise.ExamMode)
                {
                    Exercise.ExamMode = value;
                    RaisePropertyChanged("ExamMode");
                }
            }
        }

        public string FilePath { get; set; }
        public string DirectoryPath
        {
            get
            {
                return Path.GetDirectoryName(FilePath);
            }
        }

        public int NumberOfQuestions
        {
            get
            {
                return Exercise.Count;
            }
        }

        public string NextQuestionButtonText
        {
            get
            {
                if (CurrentQuestion.Index == Exercise.Questions.Count)
                {
                    return "Test beenden und Ergebnis ansehen";
                }
                else
                {
                    return "Nächste Frage";
                }
            }
        }


        public ViewModelExercise(ViewModelMain viewModelMain, Exercise exercise, string FilePath)
        {
            _ViewModelMain = viewModelMain;
            Exercise = exercise;
            Exercise.Reset();
            Window = null;
            this.FilePath = FilePath;
            CurrentQuestion = new ViewModelMultipleChoiceQuestion((MultipleChoiceQuestion) Exercise.Questions[0], 1, DirectoryPath);
            PreviousQuestionCommand = new RelayCommand(PreviousQuestion, PreviousQuestion_CanExecute);
            NextQuestionCommand = new RelayCommand(NextQuestion, NextQuestion_CanExecute);
        }

        public void Reset()
        {
            Exercise.Reset();
            CurrentQuestion = new ViewModelMultipleChoiceQuestion((MultipleChoiceQuestion)Exercise.Questions[0], 1, DirectoryPath);
        }


        void PreviousQuestion(object parameter)
        {
            CurrentQuestion = new ViewModelMultipleChoiceQuestion((MultipleChoiceQuestion)Exercise.Questions[CurrentQuestion.Index - 2], CurrentQuestion.Index - 1, DirectoryPath);
            RaisePropertyChanged("NextQuestionButtonText");
        }

        bool PreviousQuestion_CanExecute(object parameter)
        {
            if (Exercise.Questions.FindIndex(a => a == CurrentQuestion.Question) == 0)
            {
                return false;
            }
            return true;
        }

        void NextQuestion(object parameter)
        {
            if (CurrentQuestion.Index == Exercise.Questions.Count)
            {
                Result result = new Result(Exercise);
                _ViewModelMain.AddResult(result);
                var vm = new ViewModelResultDialog(result);
                var window = new ViewResult() { DataContext = vm };
                window.Show();
                Window.Close();
            }
            else
            {
                CurrentQuestion = new ViewModelMultipleChoiceQuestion((MultipleChoiceQuestion) Exercise.Questions[CurrentQuestion.Index], CurrentQuestion.Index + 1, DirectoryPath);
    
                RaisePropertyChanged("NextQuestionButtonText");
            }
        }

        bool NextQuestion_CanExecute(object parameter)
        {
            return true;
        }

        internal void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
