﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MediaTrainerGui.Helpers;
using MediaTrainerGui.Models;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Forms;
using System.IO;
using System.Xml.Serialization;

namespace MediaTrainerGui.ViewModels
{
    public class ViewModelMain : IDisposable, INotifyPropertyChanged
    {

        private FrequencyPlayer __frequencyPlayer;
        public RelayCommand PlayCommand { get; set; }
        public RelayCommand StopCommand { get; set; }
        public RelayCommand StartExerciseCommand { get; set; }
        public RelayCommand ShowResultCommand { get; set; }
        public RelayCommand OpenExercisesCommand { get; set; }
        public ObservableCollection<ViewModelExercise> Exercises { get; set; }  
        public ObservableCollection<Result> Results { get; set; }

        public int PlayingFrequency
        {
            get
            {
                return __frequencyPlayer.Frequency;
            }
            set
            {
                if (40 <= value && value <= 20000 && value != __frequencyPlayer.Frequency)
                {
                    __frequencyPlayer.Frequency = value;
                    RaisePropertyChanged("PlayingFrequency");
                }
            }
        }

        public ViewModelMain()
        {
            __frequencyPlayer = new Models.FrequencyPlayer();
            PlayCommand = new RelayCommand(Play, Player_CanPlay);
            StopCommand = new RelayCommand(Stop, Player_CanStop);
            StartExerciseCommand = new RelayCommand(StartExercise, StartExercise_CanExecute);
            ShowResultCommand = new RelayCommand(ShowResult);
            OpenExercisesCommand = new RelayCommand(OpenExercises);

            Exercises = new ObservableCollection<ViewModelExercise>();
            Results = new ObservableCollection<Result>();

            DeserializeResults();

            var args = Environment.GetCommandLineArgs();
            for (int index = 1; index < args.Length; index++)
            {
                OpenExercise(args[index]);
            }

        }

        ViewModelExercise _SelectedExercise;
        public ViewModelExercise SelectedExercise
        {
            get
            {
                return _SelectedExercise;
            }
            set
            {
                if (_SelectedExercise != value)
                {
                    _SelectedExercise = value;
                    RaisePropertyChanged("SelectedExercise");
                }
            }
        }

        Result _SelectedResult;
        public Result SelectedResult
        {
            get
            {
                return _SelectedResult;
            }
            set
            {
                if (_SelectedResult != value)
                {
                    _SelectedResult = value;
                    RaisePropertyChanged("SelectedResult");
                }
            }
        }

        void Play(object parameter)
        {
            __frequencyPlayer.IsPlaying = true;
        }

        bool Player_CanPlay(object parameter)
        {
            return __frequencyPlayer.IsPlaying == false;
        }

        void Stop(object parameter)
        {
            __frequencyPlayer.IsPlaying = false;
        }

        bool Player_CanStop(object parameter)
        {
            return __frequencyPlayer.IsPlaying == true;
        }

        public void Dispose()
        {
            __frequencyPlayer.Dispose();
        }

        void StartExercise(object parameter)
        {
            ViewModelExercise viewModelExercise = (ViewModelExercise)parameter;
            var questionDialog = new ViewExercise();
            if (viewModelExercise.ExamMode)
            {
                questionDialog.SizeToContent = SizeToContent.Manual;
                questionDialog.Topmost = true;
                questionDialog.WindowStyle = WindowStyle.None;
                questionDialog.ResizeMode = ResizeMode.NoResize;
                questionDialog.Width = SystemParameters.VirtualScreenWidth;
                questionDialog.Height = SystemParameters.VirtualScreenHeight;
                questionDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                questionDialog.WindowState = WindowState.Maximized;
            }
            viewModelExercise.Reset();
            viewModelExercise.Window = questionDialog;
            questionDialog.DataContext = viewModelExercise;
            questionDialog.ShowDialog();
        }

        bool StartExercise_CanExecute(object parameter)
        {
            if (parameter != null) {
                ViewModelExercise exercise = (ViewModelExercise)parameter;
                if (exercise.NumberOfQuestions > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public void AddResult(Result result)
        {
            SerializeResult(result);
            Results.Add(result);
            RaisePropertyChanged("Results");
        }

        void ShowResult(object parameter)
        {
            if (parameter as Result == null)
            {
                return;
            }
            var vm = new ViewModelResultDialog((Result) parameter);
            var window = new ViewResult() { DataContext = vm };
            window.Show();
        }

        void SerializeResult(Result result)
        {
            string fileName = String.Format("{0}_{1:dd-MM-yyyy_HH-mm-ss}.xml", result.Name, result.TimeStamp);
            string fileDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MediaSuite", "MediaTrainer", "Data", "Results");
            Directory.CreateDirectory(fileDirectory);
            string filePath = Path.Combine(fileDirectory, fileName);

            XmlSerializer serializer = new XmlSerializer(typeof(Result));

            using (TextWriter writer = new StreamWriter(filePath))
            {
                serializer.Serialize(writer, result);
            }

        
        }

        Result DeserializeResult(string path)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(Result));
            TextReader reader = new StreamReader(path);
            object obj = deserializer.Deserialize(reader);
            Result result = (Result)obj;
            reader.Close();
            return result;
        }

        void DeserializeResults()
        {
            Results = new ObservableCollection<Result>();
            Result result = null;
            string fileDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MediaSuite", "MediaTrainer", "Data", "Results");
            foreach(string fileName in Directory.GetFiles(fileDirectory))
            {
                result = DeserializeResult(Path.Combine(fileDirectory, fileName));
                Results.Add(result);
            }

            RaisePropertyChanged("Results");
        }

        void SerializeExercise(Exercise exercise)
        {
            string fileName = String.Format("{0}.xml", exercise.Name);
            string fileDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MediaSuite", "MediaTrainer", "Data", "Exercises");
            Directory.CreateDirectory(fileDirectory);
            string filePath = Path.Combine(fileDirectory, fileName);

            XmlSerializer serializer = new XmlSerializer(typeof(Exercise));

            using (TextWriter writer = new StreamWriter(filePath))
            {
                serializer.Serialize(writer, exercise);
            }


        }

        Exercise DeserializeExercise(string path)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(Exercise));
            TextReader reader = new StreamReader(path);
            object obj = deserializer.Deserialize(reader);
            Exercise exercise = (Exercise)obj;
            reader.Close();
            return exercise;
        }

        void OpenExercises(object parameter)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "MediaTrainer Übung (*.mte)|*.mte";
            
            dialog.Multiselect = true;
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;
            
            string[] filePaths = dialog.FileNames;

            foreach (var filePath in filePaths)
            {
                OpenExercise(filePath);
            }
        }

        void OpenExercise(string filePath)
        {
            if (filePath == null || filePath == "")
                return;
            if (!File.Exists(filePath))
                return;
            if (!filePath.ToLower().EndsWith(".mte"))
            {
                System.Windows.Forms.MessageBox.Show("Unbekanntes Dateiformat", "Warnung");
                return;
            }
            try
            {
                Exercise exercise = DeserializeExercise(filePath);
                ViewModelExercise viewModelExercise = new ViewModelExercise(this, exercise, filePath);
                List<ViewModelExercise> exercisesToRemove = new List<ViewModelExercise>();
                foreach (var vme in Exercises)
                {
                    if (vme.FilePath == filePath)
                    {
                        exercisesToRemove.Add(vme);
                    }
                }
                foreach (var vme in exercisesToRemove)
                {
                    Exercises.Remove(vme);
                }

                Exercises.Add(viewModelExercise);
                RaisePropertyChanged("Exercises");
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("Irgendwas ist wohl beim Öffnen der Übung schief gelaufen.", "Sorry");

            }

        }


        internal void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
