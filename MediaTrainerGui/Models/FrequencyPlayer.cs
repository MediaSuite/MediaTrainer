﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;

namespace MediaTrainerGui.Models
{
    class FrequencyPlayer : IDisposable, INotifyPropertyChanged
    {

        private int __Freqency;
        private bool __Playing;
        private bool __Run;
        private Thread thread;

        public int Frequency
        {
            get
            {
                return __Freqency;
            }
            set
            {
                if (37 <= value && value <= 32767)
                {
                    if (value != __Freqency)
                    {
                        __Freqency = value;
                        RaisePropertyChanged("Frequency");
                    }
                }

            }
        }

        public FrequencyPlayer()
        {
            __Freqency = 1000;
            __Playing = false;
            __Run = true;
            thread = new Thread(new ThreadStart(__PlayFrequency))
            {
                IsBackground = true
            };
            thread.Start();
        }

        ~FrequencyPlayer()
        {
            Dispose();
        }

        public void Dispose()
        {
            __Run = false;
        }

        public bool IsPlaying
        {
            get
            {
                return __Playing;
            }
            set
            {
                if (value != __Playing)
                {
                    __Playing = value;
                    RaisePropertyChanged("IsPlaying");
                }
            }
        }


        private void __PlayFrequency()
        {
            while (__Run)
            {
                if (__Playing)
                {
                    Console.Beep(__Freqency, 1000);
                }
                else
                {
                    Thread.Sleep(10);
                }
            }

        }

        void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
