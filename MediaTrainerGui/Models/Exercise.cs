﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using MediaTrainerGui.Helper;
using System.Xml.Serialization;

namespace MediaTrainerGui.Models
{
    [XmlRoot]
    public class Exercise : IResettable
    {
        [XmlElement]
        public string Name { get ; set ; }

        private List<MultipleChoiceQuestion> __Questions;
        public List<MultipleChoiceQuestion> Questions
        {
            get
            {
                return __Questions;
            }
            set
            {
                __Questions = value;
            }

        }

        [XmlAttribute]
        public bool ExamMode { get ; set; }

        public int Count
        {
            get
            {
                return __Questions.Count;
            }
        }

        public Exercise()
        {
            __Questions = new List<MultipleChoiceQuestion>();
            ExamMode = false;
        }

        public void Reset()
        {
            foreach (Question question in __Questions)
            {
                question.Reset();
            }
        }

        public void AddQuestion(MultipleChoiceQuestion question)
        {
            __Questions.Add(question);
        }

    }
}
