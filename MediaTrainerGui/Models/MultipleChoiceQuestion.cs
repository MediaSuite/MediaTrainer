﻿using MediaTrainerGui.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MediaTrainerGui.Models
{
    public class MultipleChoiceQuestion : Question
    {
        [XmlElement]
        public List<string> PossibleAnswers;

        private int? __Answer;
        public int? Answer
        {
            get
            {
                return __Answer;
            }
            set
            {
                if (value < PossibleAnswers.Count && value >= 0 && __Answer == null && value != null)
                {
                    __Answer = value;
                }
            }
        }

        private int __CorrectAnswerIndex;
        public int CorrectAnswerIndex
        {
            get
            {
                return __CorrectAnswerIndex;
            }
            set
            {
                if (value < PossibleAnswers.Count && value >= 0)
                {
                    __CorrectAnswerIndex = value;
                }
            }
        }

        public MultipleChoiceQuestion() : base()
        {
            Text = "Wähle die richtige Option!";
            PossibleAnswers = new List<string>();
            __Answer = null;
            __CorrectAnswerIndex = -1;
        }

        public void AddPossibleAnswer(string answer)
        {
            PossibleAnswers.Add(answer);
        }

        public override QuestionResults AnswerResult()
        {
            if (__Answer == null)
            {
                return QuestionResults.Unanswered ;
            }
            else if (__Answer == CorrectAnswerIndex)
            {
                return QuestionResults.CorrectAnswer;
            }
            else
            {
                return QuestionResults.WrongAnswer;
            }
        }

        public override void Reset()
        {
            __Answer = null;
        }

    }
}
