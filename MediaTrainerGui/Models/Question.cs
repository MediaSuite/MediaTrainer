﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MediaTrainerGui.Helper;

namespace MediaTrainerGui.Models
{
    [XmlRoot]
    public abstract class Question : IResettable
    {
        [XmlElement]
        public string Text{ get; set; }

        [XmlElement]
        public string AudioPath { get; set; }

        [XmlElement]
        public string ImagePath { get; set; }

        [XmlElement]
        public string ImageCaption { get; set; }

        #region Constructors

        public Question()
        {
            Text = "Gebe die richtige Antwort!";
            AudioPath = null;
            ImagePath = null;
            ImageCaption = null;
        }

        #endregion Constructors

        public abstract void Reset();
        
        public abstract QuestionResults AnswerResult();

    }
}
