﻿using MediaTrainerGui.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MediaTrainerGui.Models
{
    [XmlRoot("Result")]
    public class Result
    {
        [XmlElement]
        public int NotAnswered { get; set; }

        [XmlElement]
        public int CorrectAnswered { get; set; }
        
        [XmlElement]
        public int FalseAnswered { get; set; }
        
        public int NumberQuestions
        {
            get
            {
                return NotAnswered + CorrectAnswered + FalseAnswered;
            }
        }

        public int Percentage
        {
            get
            {
                if (NumberQuestions == 0)
                {
                    return 0;
                }
                return Convert.ToInt16( 100.0 * Convert.ToDouble(CorrectAnswered) / Convert.ToDouble(NumberQuestions));
            }
        }

        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public DateTime TimeStamp { get; set; }

        public Result()
        {
            NotAnswered = 0;
            FalseAnswered = 0;
            CorrectAnswered = 0;
            Name = "Übung";
            TimeStamp = DateTime.Now;
        }

        public Result(Exercise exercise): this()
        {
            Name = exercise.Name;
            foreach(Question question in exercise.Questions)
            {
                switch (question.AnswerResult())
                {
                    case QuestionResults.Unanswered:
                        NotAnswered++;
                        break;
                    case QuestionResults.WrongAnswer:
                        FalseAnswered++;
                        break;
                    case QuestionResults.CorrectAnswer:
                        CorrectAnswered++;
                        break;
                }
            }
        }
    }
}
